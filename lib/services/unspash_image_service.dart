
import 'package:redux_practice/res/const.dart';
import 'package:redux_practice/models/base_network_service.dart';
import 'package:redux_practice/services/network_service.dart';

class UnsplashImageService {
  UnsplashImageService._privateConstructor();

  static final UnsplashImageService instance =
      UnsplashImageService._privateConstructor();
  static String _url = UNSPLASH_API_URL;

  Future<BaseNetworkService> getImages() async {
    var responseData =
        await NetworkService.instance.request(HttpType.get, _url, null, null);
    print(responseData.response);
    return responseData;
  }
}
