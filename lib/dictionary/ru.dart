
import 'package:redux_practice/dictionary/general_dictionary.dart';
import 'package:redux_practice/dictionary/language.dart';

final Language ru = Language(
  generalDictionary: GeneralDictionary(
    language: 'Язык',
    dark: 'Темная',
    light: 'Светлая',
    theme: 'Тема',
    firsPage: 'Первая Страница Счетчика',
    photosPage: 'Фотографии',
    secondCounterPage: 'Вторая Страница Счетчика',
    thirdCounterPage: 'Третья Страница Счетчика',
  ),
);
