
import 'package:flutter/foundation.dart';
import 'package:redux_practice/dictionary/language.dart';

class SupportedLocale {
  String locale;
  Language language;

  SupportedLocale({
    @required this.language,
    @required this.locale,
  });
}
