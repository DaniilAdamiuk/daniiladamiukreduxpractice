
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:redux_practice/dictionary/en.dart';
import 'package:redux_practice/dictionary/language.dart';
import 'package:redux_practice/dictionary/ru.dart';
import 'package:redux_practice/dictionary/supported_locales.dart';

class FlutterDictionary {
  Locale locale;
  Language language;

  static FlutterDictionary instance;

  FlutterDictionary(this.locale) {
    setNewLanguage(locale.languageCode);
  }

  static FlutterDictionary of(BuildContext context) {
    return Localizations.of<FlutterDictionary>(context, FlutterDictionary);
  }

  static void init(BuildContext context) {
    instance = FlutterDictionary.of(context);
  }

  //region [RTL LANGUAGES]
  static const List<String> _rtlLanguages = <String>[
    'ar', // Arabic
    'fa', // Farsi
    'he', // Hebrew
    'ps', // Pashto
    'ur', // Urdu
  ];

  void setNewLanguage(String locale) {
    FlutterDictionaryDelegate.supportedLanguages.forEach((supportedLanguage) {
      if (supportedLanguage.locale == locale) {
        language = supportedLanguage.language;
        this.locale = Locale(locale);
      }
    });
  }

  bool isRTL(BuildContext context) {
    return _rtlLanguages.contains(
      Localizations.localeOf(context).languageCode,
    );
  }

  static Alignment isRtlAlign() {
    if (!_rtlLanguages.contains(FlutterDictionary.instance.locale.toString())) {
      return Alignment.centerLeft;
    } else {
      return Alignment.centerRight;
    }
  }
//endregion
}

class FlutterDictionaryDelegate
    extends LocalizationsDelegate<FlutterDictionary> {
  const FlutterDictionaryDelegate();

  static List<SupportedLocale> supportedLanguages = [
    SupportedLocale(locale: 'en', language: en),
    SupportedLocale(locale: 'ru', language: ru),
  ];

  ///locales added here are supported by the dictionary, but not yet by the app
  @override
  bool isSupported(Locale locale) => supportedLanguages.any(
          (supportedLanguage) => supportedLanguage.locale == locale.languageCode);

  @override
  Future<FlutterDictionary> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<FlutterDictionary>(FlutterDictionary(locale));
  }

  @override
  bool shouldReload(LocalizationsDelegate<FlutterDictionary> old) {
    return false;
  }
}
