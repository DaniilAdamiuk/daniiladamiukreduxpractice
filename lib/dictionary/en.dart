import 'package:redux_practice/dictionary/general_dictionary.dart';
import 'package:redux_practice/dictionary/language.dart';

final Language en = Language(
  generalDictionary: GeneralDictionary(
    language: 'Language',
    dark: 'Dark',
    light: 'Light',
    theme: 'Theme',
    firsPage: 'First Counter Page',
    photosPage: 'Photos Page',
    secondCounterPage: 'Second Counter Page',
    thirdCounterPage: 'Third Counter Page',
  ),
);
