import 'package:flutter/foundation.dart';

class GeneralDictionary {
  final String language;
  final String theme;
  final String firsPage;
  final String photosPage;
  final String secondCounterPage;
  final String thirdCounterPage;
  final String dark;
  final String light;

  GeneralDictionary({
    @required this.language,
    @required this.theme,
    @required this.dark,
    @required this.light,
    @required this.firsPage,
    @required this.photosPage,
    @required this.secondCounterPage,
    @required this.thirdCounterPage,
  });
}