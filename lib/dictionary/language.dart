
import 'package:flutter/cupertino.dart';
import 'package:redux_practice/dictionary/general_dictionary.dart';

class Language {
  final GeneralDictionary generalDictionary;

  Language({
  @required this.generalDictionary,
  });
}
