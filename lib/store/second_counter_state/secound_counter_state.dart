import 'dart:collection';

import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/second_counter_state/second_counter_actions.dart';

class SecondCounterState {
  final int counter;

  SecondCounterState(this.counter);

  factory SecondCounterState.initial() => SecondCounterState(0);

  SecondCounterState reducer(dynamic action) {
    print(action.runtimeType);
    return Reducer<SecondCounterState>(
      actions: HashMap.from({
        IncrementSecondCounterAction: (dynamic action) => increment(),
        DecrementSecondCounterAction: (dynamic action) => decrement(),
        ClearSecondCounterAction: (dynamic action) => clear(),
      }),
    ).updateState(action, this);
  }

  SecondCounterState clear() {
    return SecondCounterState(0);
  }

  SecondCounterState increment() {
    return SecondCounterState(this.counter + 1);
  }

  SecondCounterState decrement() {
    return SecondCounterState(this.counter - 1);
  }
}
