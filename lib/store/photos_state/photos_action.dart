
import 'package:redux_practice/dto/image_dto.dart';

class GetImagesAction {

  GetImagesAction();
}
class SetInitImagesAction  {
  final List<ImageDto> images;
  SetInitImagesAction({this.images});
}
class SaveImagesAction{
  List<ImageDto> images;
  SaveImagesAction({this.images});
}

class SelectImageAction{
  String id;
  SelectImageAction({this.id});
}