import 'package:redux_epics/redux_epics.dart';
import 'package:redux_practice/dto/image_dto.dart';
import 'package:redux_practice/repsitories/unsplash_image_repository.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/empty_action.dart';
import 'package:redux_practice/store/photos_state/photos_action.dart';
import 'package:redux_practice/store/photos_state/photos_state.dart';
import 'package:rxdart/rxdart.dart';

class PhotosEpics {
  static final indexEpic = combineEpics<AppState>([
    getImagesEpic,
    setImagesEpic,
  ]);

  static Stream<dynamic> getImagesEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    print('GetImagesEpic');
    return actions.whereType<GetImagesAction>().switchMap(
      (action) {
        return Stream.fromFuture(
          UnsplashImageRepository.instance
              .getImages()
              .then((List<ImageDto> images) {
            if (images == null) {
              return EmptyAction();
            }
            return SaveImagesAction(
              images: images,
            );
          }),
        );
      },
    );
  }

  static Stream<dynamic> setImagesEpic(
      Stream<dynamic> actions, EpicStore<AppState> store) {
    return actions.whereType<SetInitImagesAction>().switchMap((action) {
      return Stream.fromIterable([
        SaveImagesAction(images: action.images),
      ]);
    });
  }
}
