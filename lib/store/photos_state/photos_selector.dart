import 'package:redux/redux.dart';
import 'package:redux_practice/dto/image_dto.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/photos_state/photos_action.dart';

class PhotosSelector {


  static void Function(String id) selectImage(Store <AppState> store) {
    return (String id) => store.dispatch(SelectImageAction(id: id));
  }

  static void Function() getImagesFunction(Store<AppState> store) {
    return () => store.dispatch(GetImagesAction());
  }


  static String getSelectedImageId(Store<AppState> store) {
    return store.state.photosState.selectedId;
  }

  static ImageDto getSelectedImage(Store<AppState> store) {
    return store.state.photosState.selectedImage;
  }

  static List<ImageDto> getCurrentImages(Store<AppState> store) {
    return store.state.photosState.images;
  }
}