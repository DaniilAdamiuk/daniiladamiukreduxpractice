import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_practice/store/counter_state/counter_state.dart';
import 'package:redux_practice/store/language/language_state.dart';
import 'package:redux_practice/store/photos_state/photos_epics.dart';
import 'package:redux_practice/store/photos_state/photos_state.dart';
import 'package:redux_practice/store/second_counter_state/secound_counter_state.dart';
import 'package:redux_practice/store/shared/app_bar_state.dart';
import 'package:redux_practice/store/theme/theme_state.dart';

class AppState {
  final CounterState counterState;
  final PhotosState photosState;
  final AppBarState appBarState;
  final LanguageState languageState;
  final ThemeState themeState;
  final SecondCounterState secondCounterState;

  AppState({
    @required this.counterState,
    @required this.photosState,
    @required this.appBarState,
    @required this.languageState,
    @required this.themeState,
    @required this.secondCounterState,
  });

  factory AppState.initial() => AppState(
        counterState: CounterState.initial(),
        photosState: PhotosState.initial(),
    appBarState: AppBarState.initial(),
    languageState: LanguageState.initial(),
    themeState: ThemeState.initial(),
    secondCounterState: SecondCounterState.initial(),
      );

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterState: state.counterState.reducer(action),
      photosState: state.photosState.reducer(action),
      appBarState: state.appBarState.reducer(action),
      languageState: state.languageState.reducer(action),
      themeState: state.themeState.reducer(action),
      secondCounterState: state.secondCounterState.reducer(action),
    );
  }

  static final getAppEpic = combineEpics<AppState>([
    PhotosEpics.indexEpic,
  ]);
}

class Reducer<T> {
  final String TAG = '[Reducer<$T>]';
  HashMap<dynamic, T Function(dynamic)> actions;

  Reducer({
    @required this.actions,
  }) {
    actions.forEach((key, value) {
      if (value == null) throw ('All Functions must be initialize');
    });
  }

  T updateState(dynamic action, T state) {
    if (actions.containsKey(action.runtimeType)) {
      return actions[action.runtimeType](action);
    }
    return state;
  }
}


