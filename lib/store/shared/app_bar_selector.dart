import 'package:flutter/material.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice/store/shared/app_bar_actions.dart';

class AppBarSelector {


  static void Function() getToggleColorFunction(Store<AppState> store) {
    return () => store.dispatch(ChangeAppBarColor());
  }

  static bool isOrange(Store<AppState> store) {
    return store.state.appBarState.isOrange;
  }

  static Color getCurrentColor(Store<AppState> store) {
    return  store.state.appBarState.appBarColor;
  }
}