
import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/shared/app_bar_actions.dart';

class AppBarState {
  final Color appBarColor;
  final bool isOrange;

  AppBarState (this.appBarColor, this.isOrange);

  factory AppBarState .initial() => AppBarState (Colors.grey, false);


  AppBarState reducer(dynamic action) {
    print(action.runtimeType);
    return Reducer<AppBarState>(
      actions: HashMap.from({
     ChangeAppBarColor: (dynamic action) => _toggleAppBarColor(),
      }),
    ).updateState(action, this);
  }

  AppBarState _toggleAppBarColor() {
    if(appBarColor == Colors.grey) {
    return AppBarState(Colors.orange, true);
    } else {
      return AppBarState(Colors.grey, false);
    }
  }
}