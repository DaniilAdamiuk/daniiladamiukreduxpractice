
import 'package:redux/redux.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/counter_state/counter_actions.dart';
import 'package:redux_practice/store/second_counter_state/second_counter_actions.dart';

class CounterSelector {


 static void Function() getIncrementFunction(Store<AppState> store) {
   return () => store.dispatch(IncrementAction());
 }

 static void Function() getDecrementFunction(Store<AppState> store) {
   return () => store.dispatch(DecrementAction());
 }

 static void Function() getClearFunction(Store<AppState> store) {
   return () => store.dispatch(ClearCounterAction());
 }

 static void Function() getIncrementSecondCounterFunction(Store<AppState> store) {
   return () => store.dispatch(IncrementSecondCounterAction());
 }
 static void Function() getDecrementSecondCounterFunction(Store<AppState> store) {
   return () => store.dispatch(DecrementSecondCounterAction());
 }

 static int getCurrentCounter(Store<AppState> store) {
   return  store.state.counterState.counter;
 }
 static int getCurrentSecondCounter(Store<AppState> store) {
   return  store.state.secondCounterState.counter;
 }
}