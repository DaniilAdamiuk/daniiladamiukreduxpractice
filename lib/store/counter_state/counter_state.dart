import 'dart:collection';

import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/counter_state/counter_actions.dart';

class CounterState {
  final int counter;

  CounterState (this.counter);

  factory CounterState .initial() => CounterState (0);


  CounterState reducer(dynamic action) {
    print(action.runtimeType);
    return Reducer<CounterState>(
      actions: HashMap.from({
        IncrementAction: (dynamic action) => increment(),
        DecrementAction: (dynamic action) => decrement(),
        ClearCounterAction: (dynamic action) => clear(),
      }),
    ).updateState(action, this);
  }

   CounterState clear() {
    return CounterState(0);
   }

  CounterState increment() {
    return CounterState(this.counter+1);
  }

  CounterState decrement() {
    return CounterState(this.counter - 1);
  }
}

