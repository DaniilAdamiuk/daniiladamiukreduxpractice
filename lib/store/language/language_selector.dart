import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/language/language_action.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice/store/second_counter_state/second_counter_actions.dart';

class LanguageSelector {
  static String getSelectedLocale(Store<AppState> store) {
    return store.state.languageState.locale;
  }

  static void Function(String) changeLanguage(Store<AppState> store) {
    return (String locale) {
      store.dispatch(ChangeLanguageAction(locale: locale));
      store.dispatch(IncrementSecondCounterAction());
    };
  }
}
