
import 'package:flutter/foundation.dart';

class ChangeLanguageAction  {
  final String locale;

  ChangeLanguageAction({@required this.locale});

}
