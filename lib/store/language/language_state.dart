import 'dart:collection';
import 'package:flutter/foundation.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/language/language_action.dart';

class LanguageState {
  final String locale;

  LanguageState({
    @required this.locale,
  });

  factory LanguageState.initial() {
    return LanguageState(
      locale: 'en',
    );
  }

  LanguageState copyWith({
    @required String locale,
  }) {
    return LanguageState(
      locale: locale ?? this.locale,
    );
  }

  LanguageState reducer(dynamic action) {
    return Reducer<LanguageState>(
      actions: HashMap.from({
        ChangeLanguageAction: (dynamic action) =>
            changeLanguageFunction(action as ChangeLanguageAction),
      }),
    ).updateState(action, this);
  }

  LanguageState changeLanguageFunction(ChangeLanguageAction action) {
    FlutterDictionary.instance.setNewLanguage(action.locale);
    return this.copyWith(
      locale: action.locale,
    );
  }
}
