import 'package:flutter/material.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice/store/counter_state/counter_actions.dart';
import 'package:redux_practice/store/theme/theme_actions.dart';

class ThemeSelector {
  static String getSelectedTheme(Store<AppState> store) {
    return store.state.themeState.currentTheme;
  }

  static ThemeData getThemeData(Store<AppState> store) {
    return store.state.themeState.themeData;
  }

  static void Function(String) changeTheme(Store<AppState> store) {
    return (String theme) {
      store.dispatch(ChangeThemeAction(theme: theme));
      store.dispatch(IncrementAction());
    };
  }
}
