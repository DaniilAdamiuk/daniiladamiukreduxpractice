
import 'package:flutter/foundation.dart';

class ChangeThemeAction  {
  final String theme;

  ChangeThemeAction({@required this.theme});

}
