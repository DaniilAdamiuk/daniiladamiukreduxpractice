import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/theme/theme_actions.dart';
import 'package:redux_practice/widgets/theme.dart';

class ThemeState {
  final String currentTheme;
  final ThemeData themeData;

  ThemeState({
    @required this.currentTheme,
    @required this.themeData,
  });

  factory ThemeState.initial() {
    return ThemeState(
      currentTheme: 'Light',
      themeData: lightTheme,
    );
  }

  ThemeState copyWith({
    @required String theme,
    @required ThemeData newThemeData,
  }) {
    return ThemeState(
      currentTheme:  theme ?? this.currentTheme,
      themeData: newThemeData ?? this.themeData,
    );
  }

  ThemeState reducer(dynamic action) {
    return Reducer<ThemeState>(
      actions: HashMap.from({
        ChangeThemeAction: (dynamic action) =>
            changeThemeFunction(action as ChangeThemeAction),
      }),
    ).updateState(action, this);
  }

  ThemeState changeThemeFunction(ChangeThemeAction action) {
    if (action.theme == 'Light' ) {
      return copyWith(theme: 'Light', newThemeData: lightTheme,);
    }
    return copyWith(theme: 'Dark', newThemeData: darkTheme,);
  }
}