
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice/store/language/language_selector.dart';
import 'package:redux_practice/store/theme/theme_selector.dart';

class ThemeViewModel {
  final String theme;
  final ThemeData themeData;
  final void Function(String currentTeheme) changeTheme;

  ThemeViewModel({
    @required this.theme,
    @required this.themeData,
    @required this.changeTheme,
  });

  static ThemeViewModel fromStore(Store<AppState> store) {
    return ThemeViewModel(
      theme: ThemeSelector.getSelectedTheme(store),
      themeData: ThemeSelector.getThemeData(store),
      changeTheme: ThemeSelector.changeTheme(store),
    );
  }
}
