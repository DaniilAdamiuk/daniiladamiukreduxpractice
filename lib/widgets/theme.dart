import 'package:flutter/material.dart';

ThemeData darkTheme = ThemeData.dark().copyWith(
    primaryColor: Color(0xff1f655d),
    accentColor: Color(0xff40bf7a),
    textTheme: TextTheme(
        headline6: TextStyle(color: Color(0xff40bf7a)),
        subtitle1: TextStyle(color: Color(0xff40bf7a))),
    appBarTheme: AppBarTheme(color: Color(0xff1f655d)));

ThemeData lightTheme = ThemeData.light().copyWith(
    primaryColor: Color(0xfff5f5aa),
    accentColor: Color(0xff40bf7a),
    textTheme: TextTheme(
        headline6: TextStyle(color: Colors.black54),
        subtitle2: TextStyle(color: Colors.grey),
        subtitle1: TextStyle(color: Colors.blueGrey)),
    appBarTheme: AppBarTheme(
        color: Color(0xff1f655d)));
