import 'package:flutter/foundation.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice/store/language/language_selector.dart';

class LanguageViewModel {
  final String locale;
  final void Function(String locale) changeLanguage;

  LanguageViewModel({
    @required this.locale,
    @required this.changeLanguage,
  });

  static LanguageViewModel fromStore(Store<AppState> store) {
    return LanguageViewModel(
      locale: LanguageSelector.getSelectedLocale(store),
      changeLanguage: LanguageSelector.changeLanguage(store),
    );
  }
}
