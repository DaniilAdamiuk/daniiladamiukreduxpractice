import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';
import 'package:redux_practice/models/next_page_button_model.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/ui/pages/counter_page_viewmidel.dart';
import 'package:redux_practice/widgets/language_viewmodel.dart';
import 'package:redux_practice/widgets/theme_switch.dart';
import 'package:redux_practice/widgets/theme_viewmodel.dart';

import '../ui/pages/navigation_view_mode.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 812,
      allowFontScaling: true,
    );
    return StoreConnector<AppState, NavigationViewModel>(
        converter: NavigationViewModel.fromStore,
        builder: (BuildContext context, NavigationViewModel vm) {
          return StoreConnector<AppState, CounterViewModel>(
              converter: CounterViewModel.fromStore,
              builder:
                  (BuildContext context, CounterViewModel counterViewModel) {
                return Drawer(
                  child: Column(
                    children: <Widget>[
                      SafeArea(
                        child: ThemeSwitch(),
                      ),
                      SafeArea(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            StoreConnector<AppState, LanguageViewModel>(
                              converter: LanguageViewModel.fromStore,
                              builder: (BuildContext context,
                                  LanguageViewModel viewModel) {
                                return Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 50.0),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            FlutterDictionary.instance.language
                                                .generalDictionary.language,
                                            style: TextStyle(
                                              color: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1
                                                  .color,
                                            ),
                                          ),
                                          Icon(Icons.language),
                                        ],
                                      ),
                                      StreamBuilder<Object>(
                                          stream: null,
                                          builder: (context, snapshot) {
                                            return DropdownButton<String>(
                                              value: viewModel.locale,
                                              items: FlutterDictionaryDelegate
                                                  .supportedLanguages
                                                  .map((element) {
                                                return DropdownMenuItem<String>(
                                                  value: element.locale,
                                                  child: Text(element.locale),
                                                );
                                              }).toList(),
                                              onChanged: (value) {
                                                viewModel.changeLanguage(value);
                                              },
                                            );
                                          }),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.all(50),
                            child: IconButton(
                              icon: Icon(Icons.remove),
                              color: Colors.red,
                              iconSize: 36.0.w,
                              onPressed: counterViewModel.decrement,
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            margin: EdgeInsets.all(50),
                            child: IconButton(
                              icon: Icon(Icons.add),
                              color: Colors.blue,
                              iconSize: 36.0.w,
                              onPressed: counterViewModel.increment,
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Container(
                          height: 1,
                          child: ListView.builder(
                              itemCount: NextPageButton().buttons.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  height: 600.0.h,
                                  padding: EdgeInsets.all(15.0.w),
                                  child: RaisedButton(
                                    color:
                                        NextPageButton().buttons[index].color,
                                    onPressed: () {
                                      vm.changePage(
                                          NextPageButton().buttons[index].route,
                                          NavigationType.shouldReplace);
                                    },
                                    child: Text(_buttonTitle(index),
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .textTheme
                                              .subtitle1
                                              .color,
                                        )),
                                  ),
                                );
                              }),
                        ),
                      ),
                    ],
                  ),
                );
              });
        });
  }

  String _buttonTitle(int index) {
    switch (index) {
      case 0:
        return FlutterDictionary.instance.language.generalDictionary.firsPage;
      case 1:
        return FlutterDictionary.instance.language.generalDictionary.photosPage;
      case 2:
        return FlutterDictionary
            .instance.language.generalDictionary.secondCounterPage;
      case 3:
        return FlutterDictionary
            .instance.language.generalDictionary.thirdCounterPage;
    }
  }
}
