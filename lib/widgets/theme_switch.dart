
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/widgets/theme_viewmodel.dart';


class ThemeSwitch extends StatefulWidget {
  @override
  _ThemeSwitchState createState() => _ThemeSwitchState();
}

class _ThemeSwitchState extends State<ThemeSwitch> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ThemeViewModel>(
        converter: ThemeViewModel.fromStore,
        builder: (BuildContext context,
            ThemeViewModel viewModel) {
        return SwitchListTile(
            title: Text(
              FlutterDictionary.instance.language.generalDictionary.theme,
              style: TextStyle(
                color: Theme.of(context).textTheme.subtitle1.color,
              ),
            ),
            value: _isLightTheme(viewModel.theme),
            onChanged: (bool value) {
                _changeTheme(value, viewModel);
            },
            secondary: const Icon(Icons.wb_sunny,),
        );
      }
    );
  }

  void _changeTheme(bool isLight, ThemeViewModel viewModel) {
    if(isLight) {
      viewModel.changeTheme('Light');
    }
    else viewModel.changeTheme('Dark');
  }

  bool _isLightTheme(String theme) {
    if(theme == 'Light') {
      return true;
    }
    return false;
  }
}