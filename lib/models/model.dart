import 'package:flutter/material.dart';

class Item {
  final int id;
  final String body;

  Item({
    @required this.id,
    @required this.body,
  });

  Item copyWith({int id, String body}) {
    return Item(
      id: id ?? this.id,
      body: body ?? this.body,
    );
  }

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      id: int.parse(json["id"]),
      body: json["body"],
    );


  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "body": this.body,
    };
  }
//

}

