import 'package:flutter/material.dart';
import 'package:redux_practice/res/const.dart';

class NextPageButton {
  final String route;
  final String title;
  final Color color;

  NextPageButton({this.route, this.title, this.color});

  static List<NextPageButton> _buttons = [
    NextPageButton(
      route: FIRST_PAGE_ROUTE,
      title: TO_FIRST_PAGE_TITLE,
      color: Colors.white,
    ),
    NextPageButton(
      route: SECOND_PAGE_ROUTE,
      title: TO_SECOND_PAGE_TITLE,
      color: Colors.blue,
    ),
    NextPageButton(
      route: THIRD_PAGE_ROUTE,
      title: TO_THIRD_PAGE_TITLE,
      color: Colors.green,
    ),
    NextPageButton(
      route: FOURTH_PAGE_ROUTE,
      title: TO_FOURTH_PAGE_TITLE,
      color: Colors.yellow,
    ),
  ];

  List<NextPageButton> get buttons{
    return[..._buttons];
  }
}
