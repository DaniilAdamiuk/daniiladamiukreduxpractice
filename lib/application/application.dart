import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';

import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/widgets/language_viewmodel.dart';
import 'package:redux_practice/widgets/theme_viewmodel.dart';

import '../helpers/route_helper.dart';

class MyApp extends StatefulWidget {

  final Store store;

  MyApp(this.store);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {

    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    NavigatorObserver navigatorObserver = NavigatorObserver();
    return StoreProvider<AppState>(
      store: widget.store,
      child:  StoreConnector<AppState, ThemeViewModel>(
          converter: ThemeViewModel.fromStore,
          builder: (BuildContext context,
              ThemeViewModel themeViewModel) {
              return  StoreConnector<AppState, LanguageViewModel>(
                  converter: LanguageViewModel.fromStore,
                  builder: (BuildContext context, LanguageViewModel viewModel) {
                  return MaterialApp(
                    theme: themeViewModel.themeData,
                    title: 'Flutter Demo',
                    navigatorObservers: [
                      navigatorObserver,
                    ],
                    navigatorKey: NavigatorHolder.navigatorKey,
                    onGenerateTitle: (BuildContext context) {
                      FlutterDictionary.init(context);
                      return FlutterDictionary
                          .instance.language.generalDictionary.firsPage;
                    },
                    localizationsDelegates: [
                      const FlutterDictionaryDelegate(),
                      GlobalWidgetsLocalizations.delegate,
                      GlobalMaterialLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate,
                    ],
                    locale: Locale(viewModel.locale),
                    supportedLocales: FlutterDictionaryDelegate.supportedLanguages.map<Locale>(
                          (element) => Locale(element.locale),
                    ),
                    onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
                  );
                }
              );
            }
          ),


    );
  }
}