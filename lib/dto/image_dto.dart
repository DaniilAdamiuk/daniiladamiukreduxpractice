
import 'package:flutter/foundation.dart';
import 'package:redux_practice/dto/image_urls_dto.dart';
import 'package:redux_practice/dto/user_unsplash_dto.dart';

class ImageDto {
  final String id;
  final String createdAt;
  final String description;
  final String altDescription;
  final ImageUrlsDto imageUrls;
  final UserUnsplashDto user;

  ImageDto({
    @required this.id,
    @required this.createdAt,
    @required this.description,
    @required this.altDescription,
    @required this.imageUrls,
    @required this.user,
  });

  factory ImageDto.fromJson(Map<String, dynamic> json) {
    return ImageDto(
      id: json["id"],
      createdAt: json["createdAt"],
      description: json["description"],
      altDescription: json["alt_description"],
      imageUrls: ImageUrlsDto.fromJson(json["urls"]),
      user: UserUnsplashDto.fromJson(json["user"]),
    );
  }
}