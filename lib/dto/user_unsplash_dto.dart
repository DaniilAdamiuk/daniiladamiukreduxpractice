
import 'package:flutter/material.dart';
import 'package:redux_practice/dto/unsplash_profile_image_dto.dart';

class UserUnsplashDto {
  final String name;
  final String bio;
  final String location;
  final String portfolioUrl;
  final String instagramUsername;
  final UnsplashProfileImageDto profileImage;

  UserUnsplashDto({
    @required this.name,
    @required this.bio,
    @required this.location,
    @required this.portfolioUrl,
    @required this.instagramUsername,
    @required this.profileImage,
  });

  factory UserUnsplashDto.fromJson(Map<String, dynamic> json) {
    return UserUnsplashDto(
      name: json["name"],
      bio: json["bio"],
      location: json["location"],
      portfolioUrl: json['portfolio_url'],
      instagramUsername: json['instagram_username'],
      profileImage: UnsplashProfileImageDto.fromJson(json["profile_image"]),
    );
  }
//

}
