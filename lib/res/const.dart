import 'package:redux_practice/dictionary/flutter_dictionary.dart';

const String UNSPLASH_API_URL = 'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

const String FIRST_PAGE_ROUTE = '/firs-page';
const String SECOND_PAGE_ROUTE = '/second-page';
const String THIRD_PAGE_ROUTE = '/third-page';
const String FOURTH_PAGE_ROUTE = '/fourth-page';
const String IMAGE_DETAIL_ROUTE = '/image-details';
const String PHOTOGRAPHER_DETAIL_ROUTE = '/photographer-details';


 String TO_FIRST_PAGE_TITLE = FlutterDictionary.instance.language.generalDictionary.firsPage;
 String TO_SECOND_PAGE_TITLE = FlutterDictionary.instance.language.generalDictionary.photosPage;
 String TO_THIRD_PAGE_TITLE = FlutterDictionary.instance.language.generalDictionary.secondCounterPage;
 String TO_FOURTH_PAGE_TITLE = FlutterDictionary.instance.language.generalDictionary.thirdCounterPage;


const String EMPTY_STRING = '';
const String INSTAGRAM_BASE_URL = 'https://www.instagram.com/';