
import 'package:redux_practice/dto/image_dto.dart';
import 'package:redux_practice/services/unspash_image_service.dart';

class UnsplashImageRepository {
  UnsplashImageRepository._privateConstructor();

  static final UnsplashImageRepository instance =
      UnsplashImageRepository._privateConstructor();

  Future<List<ImageDto>> getImages() async {
    var responseData = await UnsplashImageService.instance.getImages();
    List<ImageDto> images = [];
    for (var item in responseData.response) {
      images.add(ImageDto.fromJson(item));
    }
    return images;
  }
}
