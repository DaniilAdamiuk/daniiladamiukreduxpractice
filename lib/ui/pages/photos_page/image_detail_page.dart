
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice/dto/image_dto.dart';
import 'package:redux_practice/res/const.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/ui/layouts/main_layout.dart';
import 'package:redux_practice/ui/pages/navigation_view_mode.dart';
import 'package:redux_practice/ui/pages/photos_page/photos_page_viewmodel.dart';

class ImageDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return  StoreConnector<AppState, PhotosPageViewModel>(
        converter: PhotosPageViewModel.fromStore,
        builder: (BuildContext context, PhotosPageViewModel viewModel) {
          ScreenUtil.init(
            context,
            width: 375,
            height: 812,
            allowFontScaling: true,
          );
         final ImageDto image = viewModel.selectedImage;
        return MainLayout(
          child: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: Colors.blueGrey,
                  expandedHeight: 250.0.h,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text(
                      image.description ?? image.user.name,
                      style: TextStyle(color: Colors.white, fontSize: 16.h),
                    ),
                    background: Hero(
                      tag: viewModel.selectedImageId,
                      child: Image.network(
                        image.imageUrls.regular,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.0.w),
                        width: double.infinity,
                        child: ListTileTheme(
                          textColor: Colors.black ,
                          child: StoreConnector<AppState, NavigationViewModel>(
                              converter: NavigationViewModel.fromStore,
                              builder: (BuildContext context, NavigationViewModel navigationVM) {
                              return ListTile(
                                onTap: () {
                                  navigationVM.changePage(PHOTOGRAPHER_DETAIL_ROUTE, NavigationType.shouldPush);
                                },
                                title: Text(
                                  image.user.name,
                                ),
                                subtitle: Text(
                                  image.user.location ?? image.user.bio ?? EMPTY_STRING,
                                ),
                                leading: Hero(
                                  tag: image.user.name,
                                  child: Image.network(
                                    image.user.profileImage.large,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              );
                            }
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0.w),
                      Container( padding: const EdgeInsets.all(10.0),
                        child: Text(
                          image.user.bio ?? EMPTY_STRING,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0.h,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 20.0.w,
                      ),
                    ],
                  ),
                ),
              ],
            ),
        );
      }
    );
  }
}
