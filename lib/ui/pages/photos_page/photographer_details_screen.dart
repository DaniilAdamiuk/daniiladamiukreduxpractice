import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice/dto/image_dto.dart';
import 'package:redux_practice/res/const.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/ui/layouts/main_layout.dart';
import 'package:redux_practice/ui/pages/photos_page/photos_page_viewmodel.dart';
import 'package:url_launcher/url_launcher.dart';

class PhotographerDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PhotosPageViewModel>(
        converter: PhotosPageViewModel.fromStore,
        builder: (BuildContext context, PhotosPageViewModel viewModel) {
          ScreenUtil.init(
            context,
            width: 375,
            height: 812,
            allowFontScaling: true,
          );
          final ImageDto image = viewModel.selectedImage;
        return MainLayout(
          child:CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: Colors.blueGrey ,
                  expandedHeight: 250.0.h,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    titlePadding: EdgeInsets.zero,
                    centerTitle: true,
                    title: Text(
                      image.user.name,
                      style: TextStyle(color: Colors.white, fontSize: 20.0.h),
                    ),
                    background: Hero(
                      tag: viewModel.selectedImageId,
                      child: Center(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100.0.w),
                          child: Image.network(
                            image.user.profileImage.large,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      Container(
                        margin: const EdgeInsets.all(10.0),
                        child: Text(
                          image.user.bio ?? image.user.location ?? EMPTY_STRING,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0.w,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      image.user.portfolioUrl == null? SizedBox() : ListTileTheme(
                        textColor: Colors.black,
                        iconColor: Colors.black,
                        dense: true,
                        child: InkWell(
                          onTap: () async {
                            if(await canLaunch(image.user.portfolioUrl)) {
                               await launch(image.user.portfolioUrl);
                            }
                          },
                          child: ListTile(
                            leading: Icon(Icons.work,),
                            title: Text('Portfolio:', style: TextStyle(fontSize: 16.0.w),),
                            subtitle: Text(image.user.portfolioUrl),
                          ),
                        ),
                      ),
                      image.user.instagramUsername == null? SizedBox() : ListTileTheme(
                        textColor: Colors.black,
                        iconColor: Colors.black,
                        dense: true,
                        child: InkWell(
                          onTap: () async{
                            if(await canLaunch(INSTAGRAM_BASE_URL+image.user.instagramUsername)){
                              await launch(INSTAGRAM_BASE_URL+image.user.instagramUsername);
                            }
                          },
                          child: ListTile(
                            leading: Icon(Icons.photo_camera),
                            title: Text('Instagram:', style: TextStyle(fontSize: 16.0.w)),
                            subtitle: Text(image.user.instagramUsername),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
        );
      }
    );
  }
}
