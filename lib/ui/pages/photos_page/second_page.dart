import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';
import 'package:redux_practice/res/const.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/ui/layouts/main_layout.dart';
import 'package:redux_practice/ui/pages/navigation_view_mode.dart';
import 'package:redux_practice/ui/pages/photos_page/photos_page_viewmodel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice/widgets/drawer.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 812,
      allowFontScaling: true,
    );
    return StoreConnector<AppState, PhotosPageViewModel>(
        converter: PhotosPageViewModel.fromStore,
        onInitialBuild: (PhotosPageViewModel viewModel) =>
            viewModel.getImages(),
        builder: (BuildContext context, PhotosPageViewModel viewModel) {
          return MainLayout(
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.blueGrey,
                title: Text(
                  FlutterDictionary
                      .instance.language.generalDictionary.photosPage,
                ),
              ),
              drawer: CustomDrawer(),
              body: Container(
                child: ListView.builder(
                  itemBuilder: (BuildContext ctx, int i) => Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0.w),
                      child: Container(
                        height: 250.0.w,
                        margin: const EdgeInsets.all(10.0),
                        child: GridTile(
                          header: GridTileBar(
                            backgroundColor: Colors.black87,
                            title: Text(
                              viewModel.images[i].altDescription == null
                                  ? EMPTY_STRING
                                  : viewModel.images[i].altDescription,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            ),
                          ),
                          child: StoreConnector<AppState, NavigationViewModel>(
                              converter: NavigationViewModel.fromStore,
                              builder: (BuildContext context,
                                  NavigationViewModel navigationVM) {
                                return GestureDetector(
                                  onTap: () {
                                    viewModel
                                        .selectImage(viewModel.images[i].id);
                                    navigationVM.changePage(IMAGE_DETAIL_ROUTE,
                                        NavigationType.shouldPush);
                                  },
                                  child: Hero(
                                    tag: viewModel.images[i].id,
                                    child: Image.network(
                                      viewModel.images[i].imageUrls.regular,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ),
                    ),
                  ),
                  itemCount: viewModel.images.length,
                ),
              ),
            ),
          );
        });
  }
}
