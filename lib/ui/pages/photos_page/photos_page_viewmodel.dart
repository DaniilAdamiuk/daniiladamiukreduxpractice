
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice/dto/image_dto.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/photos_state/photos_selector.dart';

class PhotosPageViewModel {
  final List<ImageDto> images;
  final ImageDto selectedImage;
  final String selectedImageId;
  final void Function() getImages;
  final void Function(String id) selectImage;


  PhotosPageViewModel({
    @required this.images,
    @required this.selectedImage,
    @required this.selectedImageId,
    @required this.getImages,
    @required this.selectImage,

  });

  static PhotosPageViewModel fromStore(Store<AppState> store) {
    return PhotosPageViewModel(
      images: PhotosSelector.getCurrentImages(store),
      selectedImage: PhotosSelector.getSelectedImage(store),
      selectedImageId: PhotosSelector.getSelectedImageId(store),
      getImages: PhotosSelector.getImagesFunction(store),
      selectImage: PhotosSelector.selectImage(store),
    );
  }
}