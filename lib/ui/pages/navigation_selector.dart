import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_practice/helpers/route_helper.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux/redux.dart';

class NavigationSelector {
  static void Function(String route, NavigationType type) navigateToNextPage(
      Store<AppState> store) {
    return (String route, NavigationType type) {
      if (type == NavigationType.shouldReplace) {
        store.dispatch(NavigateToAction.replace(route));
      } else {
        store.dispatch(NavigateToAction.push(route));
      }
    };
  }
}