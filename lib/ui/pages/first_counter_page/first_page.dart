import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';
import 'package:redux_practice/ui/layouts/main_layout.dart';
import 'package:redux_practice/widgets/counter.dart';
import 'package:redux_practice/widgets/drawer.dart';
import 'package:redux_practice/store/appstate/app_state.dart';

import '../counter_page_viewmidel.dart';

class FirstCounterPage extends StatelessWidget {
  final int counter = 0;
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterViewModel>(
        converter: CounterViewModel.fromStore,
        builder: (BuildContext context, CounterViewModel vm) {
          ScreenUtil.init(
            context,
            width: 375,
            height: 812,
            allowFontScaling: true,
          );
          return MainLayout(
            child: Scaffold(
                appBar: AppBar(
                  backgroundColor: vm.appBarColor,
                  title: Text(FlutterDictionary.instance.language.generalDictionary.firsPage,),
                ),
                drawer: CustomDrawer(),
                body: Container(
                  color: Colors.blueGrey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                     Counter(counterValue: vm.counter.toString(),),
                    ],
                  ),
                )),
          );
        });
  }
}
