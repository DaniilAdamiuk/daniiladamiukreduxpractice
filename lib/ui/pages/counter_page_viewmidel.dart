import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter/foundation.dart';

import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/store/counter_state/counter_selector.dart';
import 'package:redux_practice/store/shared/app_bar_selector.dart';

class CounterViewModel {
  final int counter;
  final int secondCounter;
  final bool isOrange;
  final Color appBarColor;
  final void Function() changeColor;
  final void Function() increment;
  final void Function() decrement;
  final void Function() incrementSecond;
  final void Function() decrementSecond;
  final void Function() clear;

  CounterViewModel({
    @required this.counter,
    @required this.secondCounter,
    @required this.appBarColor,
    @required this.changeColor,
    @required this.increment,
    @required this.decrement,
    @required this.incrementSecond,
    @required this.decrementSecond,
    @required this.clear,
    @required this.isOrange,
  });

  static CounterViewModel fromStore(Store<AppState> store) {
    return CounterViewModel(
      counter: CounterSelector.getCurrentCounter(store),
      secondCounter: CounterSelector.getCurrentSecondCounter(store),
      isOrange: AppBarSelector.isOrange(store),
      appBarColor: AppBarSelector.getCurrentColor(store),
      changeColor: AppBarSelector.getToggleColorFunction(store),
      increment: CounterSelector.getIncrementFunction(store),
      decrement: CounterSelector.getDecrementFunction(store),
      incrementSecond: CounterSelector.getDecrementSecondCounterFunction(store),
      decrementSecond: CounterSelector.getDecrementSecondCounterFunction(store),
      clear: CounterSelector.getClearFunction(store),
    );
  }
}
