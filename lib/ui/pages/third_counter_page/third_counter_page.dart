import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_practice/dictionary/flutter_dictionary.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux_practice/ui/layouts/main_layout.dart';
import 'package:redux_practice/ui/pages/counter_page_viewmidel.dart';
import 'package:redux_practice/widgets/counter.dart';
import 'package:redux_practice/widgets/drawer.dart';

class ThirdCounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      child: StoreConnector<AppState, CounterViewModel>(
          converter: CounterViewModel.fromStore,
          builder: (BuildContext context, CounterViewModel vm) {
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  FlutterDictionary
                      .instance.language.generalDictionary.thirdCounterPage,
                ),
              ),
              drawer: CustomDrawer(),
              body: Container(
                color: Colors.yellow,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Counter(
                      counterValue: (vm.counter * 10).toString(),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
