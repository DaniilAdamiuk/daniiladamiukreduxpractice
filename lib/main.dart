import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux_practice/application/application.dart';
import 'package:redux_practice/store/appstate/app_state.dart';
import 'package:redux/redux.dart';

void main() {

  final store = new Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      EpicMiddleware<AppState>(AppState.getAppEpic),
      NavigationMiddleware<AppState>(),
    ],
  );


  runApp(MyApp(store));
}

